import express, {Application, Request, Response} from 'express';
import cors from 'cors';
import compression from 'compression';
import {config as dotenv} from 'dotenv';
import morgan from 'morgan';


import VehicleRoutes from './routers/VehicleRoutes';


class App{
    public app:Application;
    constructor(){
        this.app = express();
        this.plugins();
        this.routes();
        dotenv();
    }

    protected plugins(){
        this.app.use(express.json());
        this.app.use(morgan("dev"));
        this.app.use(compression());
        this.app.use(cors());

    }

    protected routes(){
        this.app.use("/api/vehicles", VehicleRoutes);
    }
}

const app = new App().app;

const PORT:number = Number(process.env.PORT);

app.listen(PORT, ()=>{
    console.log('Server is running on port : ' + PORT);
})




