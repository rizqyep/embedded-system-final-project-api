import {Router, Request, Response} from 'express';
import VehicleController from '../controllers/VehicleController';

class VehicleRoutes{
    public router:Router;
    constructor(){
        this.router = Router();
        this.routes();
    }

    public routes():void{
        this.router.post('/registration',VehicleController.registration);
        this.router.post("/:uid",VehicleController.readData);
    }
}

export default new VehicleRoutes().router;