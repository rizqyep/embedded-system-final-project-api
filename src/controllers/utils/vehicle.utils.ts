import {FData, Vehicle,Owner} from '../../utils/data.types';
import {notifyComingDue, notifyPastDue} from '../../utils/mail';
export const validateData = async(vehicle:FData<Vehicle>, owner:FData<Owner>)=>{
    const taxDue = new Date(vehicle.data.tax_due);
    const date = new Date().toISOString();
    const dateToday = new Date(date.split('T')[0]);
    const differenceInTime = taxDue.getTime() - dateToday.getTime();
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);
    
    if(differenceInDays <= 30 && differenceInDays >=1){
        notifyComingDue(owner.data.email, differenceInDays, vehicle.data.plate_num);
    }
    else if (differenceInDays < 0){
        notifyPastDue(owner.data.email, differenceInDays * -1, vehicle.data.plate_num);
    }
}

export const getDayDifference = (vehicle:FData<Vehicle>):Number =>{
    const taxDue = new Date(vehicle.data.tax_due);
    const date = new Date().toISOString();
    const dateToday = new Date(date.split('T')[0]);
    const differenceInTime = taxDue.getTime() - dateToday.getTime();
    const differenceInDays = differenceInTime / (1000 * 3600 * 24);
    return differenceInDays;
}