import {Request, Response} from 'express';
import {q, server} from '../utils/fauna';
import {get_by_index, extract_ref_id,check_exists} from '../utils/fauna_utils';
import {FData, Vehicle,Owner} from '../utils/data.types';
import {validateData,getDayDifference} from './utils/vehicle.utils';
class VehicleController{
    registration = async(req:Request, res: Response):Promise<Response> => {
        try{
        const {uid,type,plate_num,tax_due,owner_email} = req.body;

        const userExist = await check_exists({index:'by_email',identifier:owner_email,collection:'users'});
        if(!userExist){
            return res.status(400).json({type:'ERR_USER_NOT_FOUND',message:'User not found!'});
        }
        const vehicleExist = await check_exists({index:'by_uid',identifier:uid,collection:'vehicles'});
        if(vehicleExist){
            return res.status(400).json({type:'ERR_VEHICLE_REGISTERED',message:'Vehicle already registered!'});
        }

        const expr = get_by_index('users_by_email',owner_email);
        const owner:FData<Owner> = await server.query(expr);
const registeredUser: FData<Vehicle> = await server.query(
			q.Create(q.Collection('vehicles'), {
				data: {
					uid,
                    type,
                    plate_num,
                    owner:owner.ref.id,
                    tax_due
				},
			})
		);

        return res.status(200).json({status:'SUCCESS'});}
        catch(err){
            console.log(err);
            return res.status(500).json({message:err.message,status:'ERROR'});
        }
    }

    readData = async(req:Request, res:Response):Promise<Response> => {
        try{
        const {uid} = req.params;
        const expr = get_by_index('vehicles_by_uid',uid);
        const vehicle:FData<Vehicle> = await server.query(expr);
        
        const owner:FData<Owner> = await server.query(
            q.Get(q.Ref(
                q.Collection('users'),vehicle.data.owner)
            ));
        

        validateData(vehicle,owner);
       
        
        
        
        const responseData = {
            vehicle : extract_ref_id(vehicle),
            owner : extract_ref_id(owner),
            remainingDay:  getDayDifference(vehicle)
        }
        return res.json(
            responseData
        );        
        }
        catch(err){
            return res.status(400).json({error:err});
        }
    }
}


export default new VehicleController();
