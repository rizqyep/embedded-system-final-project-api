export interface FArray<T> {
	data: Array<T>;
}
export interface FData<T> {
	ref: { id: string };
	ts: number;
	data: T;
}


export interface Vehicle{
    uid : string;
    type: string;
    plate_num : string;
    tax_due: string;
    owner: string;
}

export interface Owner{
    name : string;
    address: string;
    phone_number: string;
    email: string;
}