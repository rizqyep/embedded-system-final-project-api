const nodemailer = require("nodemailer");

const transporter = nodemailer.createTransport({
    service:'gmail',
    host:'smtp@gmail.com',
    auth:{
        user : 'taxnotifier.embedded.group2@gmail.com',
        pass: 'taxnotifier123'
    }
})


export function notifyComingDue(toMail:string, dueDay:number,plate_num:string):void{
    const mailOptions = {
        from : 'Informasi Pajak',
        to : toMail,
        subject : 'Pajak Kendaraan Anda Akan Segera Jatuh Tempo',
        text : `Berdasarkan pembacaan sensor kami, pajak kendaraan anda dengan Plat Nomor ${plate_num} akan segera jatuh tempo dalam ${dueDay} hari, mohon segera lakukan pembayaran.`
    }
    transporter.sendMail(mailOptions, function(error:unknown,info:object){
        if(error){
            console.log(error);
        }
        else{
            console.log('Email sent : ' + info);
        }
    })
}


export function notifyPastDue(toMail:string, dueDay:number,plate_num:string):void{
     const mailOptions = {
        from : 'Informasi Pajak',
        to : toMail,
        subject : 'Pajak Kendaraan Anda Sudah Jatuh Tempo',
        html : `
        Berdasarkan pembacaan sensor kami, pajak kendaraan anda dengan 
        Plat Nomor ${plate_num} sudah jatuh tempo selama ${dueDay} hari, mohon segera lakukan pembayaran.`
    }
    transporter.sendMail(mailOptions, function(error:unknown,info:object){
        if(error){
            console.log(error);
        }
        else{
            console.log('Email sent : ' + info);
        }
    })
}