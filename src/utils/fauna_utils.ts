import type { Expr } from 'faunadb';
import type { FData } from './data.types';
import { q, server } from './fauna';

export const check_collection_exists = async (collection_name: string): Promise<boolean> => {
	try {
		return server.query(q.Exists(q.Collection(collection_name)));
	} catch (error) {
		throw new Error(error.message);
	}
};

/**
 * Check existence of a document
 * @param params contains index, identifier, and collection
 * @param index (optional) index name, fill this parameter if you want to check by index
 * @param identifier index terms value or id
 * @param collection collection's name
 * @returns true if exist and vice versa
 */
export async function check_exists(params: {
	index?: string;
	identifier: string | string[];
	collection: string;
}): Promise<boolean> {
	try {
		const { collection, identifier, index } = params;

		if (index) {
			// check existence using index
			const index_name = `${collection}_${index}`;
			return server.query(q.Exists(q.Match(q.Index(index_name), identifier)));
		} else {
			// check existence using id
			return server.query(q.Exists(q.Ref(q.Collection(collection), identifier)));
		}
	} catch (error) {
		throw new Error(error.message);
	}
}

/**
 * Get document by index
 * @param index index name
 * @param identifier index terms value
 * @returns Fauna Expr, must be used inside q.server(q.Query())
 */
export function get_by_index(index: string, identifier: string): Expr {
	return q.Get(q.Match(q.Index(index), identifier));
}

/**
 * Extract ref id
 * @param document single document from fauna
 * @returns object contains id and the rest of data from fauna document
 */
export function extract_ref_id<T>({ ref, data }: FData<T>): T {
	return { id: ref.id, ...data };
}
