import 'dotenv/config';
import { query, Client } from 'faunadb';

export const q = query;
export const server = new Client({ secret: process.env.FAUNA_SECRET || '' });
export const client = (token: string): Client => new Client({ secret: token });
